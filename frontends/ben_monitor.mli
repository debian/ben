open Ben

val frontend : Frontend.frontend

type output_format = Text | Xhtml | Levels | Json

val string_of_format : output_format -> string
val format_of_string : string -> output_format

type transition_data = {
  config : Types.expr Core.StringMap.t;
  monitor_data :
    (Package.source Package.t * (string * (string * Types.status)) list) list
    list;
  sources : (Package.source, Package.source Package.t) Package.Map.t;
  binaries : Package.binary Package.t Modules.PAMap.t;
  rounds : Package.source Package.Name.t list list;
  dep_graph : (Package.source, Package.source Package.Set.t) Package.Map.t;
  all : int;
  bad : int;
  packages : Package.source Data.S.t;
}

val compute_transition_data :
  Modules.Marshallable.t -> Types.expr Core.StringMap.t -> transition_data

val print_html_monitor :
  Template.t ->
  transition_data ->
  [< Html_types.div_content_fun > `Table ] Tyxml.Html.elt option ->
  Tyxml.Html.doc

val print_monitor : output_format -> Format.formatter -> transition_data -> unit
val baseurl : string ref

val a_link :
  string -> string Tyxml.Html.wrap -> [> `A of [> `PCDATA ] ] Tyxml.Html.elt

val generated_on_text :
  unit -> [> `A of [> `PCDATA ] | `PCDATA ] Tyxml.Html.elt list

val check_media_dir : string -> unit
