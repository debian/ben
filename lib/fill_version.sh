#!/bin/sh

VERSION="$(dpkg-parsechangelog -S Version)"
: ${SOURCE_DATE_EPOCH:="$(date +%s)"}
BUILD_DATE="$(date -u -d "@$SOURCE_DATE_EPOCH" +"%F %T %Z")"

exec sed \
    -e "s/@VERSION@/$VERSION/g" \
    -e "s/@BUILD_DATE@/$BUILD_DATE/g" \
    "$@"
