(**************************************************************************)
(*  Copyright © 2009-2024 Stéphane Glondu <steph@glondu.net>              *)
(*            © 2010-2013 Mehdi Dogguy <mehdi@dogguy.org>                 *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

type t = string

external verrevcmp : string -> string -> int = "caml_verrevcmp"

let decomp =
  let rex = Re.Pcre.regexp "^(?:(\\d+):)?(?:([^\\s-]+)|(\\S+)-([^\\s-]+))$" in
  fun x ->
    try
      let r = Re.Pcre.exec ~rex x in
      let epoch =
        try int_of_string (Re.Pcre.get_substring r 1) with Not_found -> 0
      in
      let upstream =
        try Re.Pcre.get_substring r 2
        with Not_found -> Re.Pcre.get_substring r 3
      in
      let debian = try Re.Pcre.get_substring r 4 with Not_found -> "0" in
      (epoch, upstream, debian)
    with Not_found ->
      Printf.ksprintf invalid_arg "invalid version number: %s" x

let compare x y =
  let x1, x2, x3 = decomp x and y1, y2, y3 = decomp y in
  let ( >>= ) x f = if x = 0 then f () else x in
  let cmp x y () = verrevcmp x y in
  x1 - y1 >>= cmp x2 y2 >>= cmp x3 y3

let compare_predicate cmp x y =
  let d = compare x y in
  match cmp with
  | Types.Eq -> d = 0
  | Ge -> d >= 0
  | Gt -> d > 0
  | Le -> d <= 0
  | Lt -> d < 0
