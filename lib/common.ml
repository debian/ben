(**************************************************************************)
(*  Copyright © 2009-2024 Stéphane Glondu <steph@glondu.net>              *)
(*            © 2010-2013 Mehdi Dogguy <mehdi@dogguy.org>                 *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU Affero General Public License as        *)
(*  published by the Free Software Foundation, either version 3 of the    *)
(*  License, or (at your option) any later version, with the additional   *)
(*  exemption that compiling, linking, and/or using OpenSSL is allowed.   *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful, but   *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of            *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *)
(*  Affero General Public License for more details.                       *)
(*                                                                        *)
(*  You should have received a copy of the GNU Affero General Public      *)
(*  License along with this program.  If not, see                         *)
(*  <http://www.gnu.org/licenses/>.                                       *)
(**************************************************************************)

let choose_escape str xs =
  let rec loop = function
    | c :: cs -> if String.contains str c then loop cs else c
    | _ -> Stdlib.raise Not_found
  in
  loop xs

let string_of_regexp (regexp, _) =
  let escape = choose_escape regexp [ '/'; '@'; ','; '%' ] in
  if escape = '/' then Printf.sprintf "/%s/" regexp
  else Printf.sprintf "@%c%s%c" escape regexp escape

let string_of_cmp = function
  | Types.Le -> "<="
  | Lt -> "<<"
  | Eq -> "="
  | Gt -> ">>"
  | Ge -> ">="

let string_of_string escaping string =
  if escaping then
    let escape = choose_escape string [ '"'; '\'' ] in
    Printf.sprintf "%c%s%c" escape string escape
  else string

let string_of_status = function
  | Types.Unknown -> " "
  | Up_to_date -> "✔"
  | Outdated -> "✘"
  | Partial -> "⁈"

let class_of_status = function
  | Types.Unknown -> "unknown"
  | Up_to_date -> "good"
  | Outdated -> "bad"
  | Partial -> "partial"
