#!/usr/bin/env python3

import sys
import yaml

debcheck = yaml.safe_load(sys.stdin)
broken_packages = [p["package"] for p in debcheck["report"] if p["status"] == "broken"]
broken_packages.sort()

for p in broken_packages:
    print(p)
