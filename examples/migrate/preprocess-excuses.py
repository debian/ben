#!/usr/bin/env python3

import sys
import yaml
import json

excuses = yaml.safe_load(open(sys.argv[1]))
result = {}

for source in excuses["sources"]:
    result[source["item-name"]] = {
        "verdict": source["migration-policy-verdict"],
        "excuses": source["excuses"]
    }

json.dump(result, sys.stdout)
