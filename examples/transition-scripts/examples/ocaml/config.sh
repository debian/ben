: ${MIRROR:="http://deb.debian.org/debian"}
: ${ORIGIN:="ocaml.debian.net"}
: ${LABEL:="ocaml-transition"}
: ${SUITE:="unstable-ocaml"}
: ${SUFFIX:="+ocaml1"}
: ${SOURCE_SUITE:="unstable"}
: ${BASE_SUITE:="unstable"}
: ${URL_PREFIX:="http://ocaml.debian.net/transitions"}
