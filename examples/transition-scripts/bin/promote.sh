#!/bin/sh

PACKAGE="$1"

mkdir -p pool/$PACKAGE
dcmd mv -vi build/${PACKAGE}_*.changes build/${PACKAGE}_*.build pool/$PACKAGE
