Transition scripts
==================

Introduction
------------

The goal is to build a new universe with a new version of some
package, typically a compiler or a library. These scripts were
originally created to prepare OCaml transitions, which require strict
ordering of the recompilations.

Warning: these scripts use sbuild with unshare backend. They do not
need root privileges but require unprivileged user namespaces.

Warning: by default, `/tmp` is used as temporary directory for the
builds. If space there is limited, point `$TMPDIR` to a suitable
value.

Dependencies
------------

    apt install sbuild mmdebstrap uidmap devscripts ben

Guide
-----

To start a new transition, run:

    bin/create-transition.sh $NAME $BASE $CONFIG

where:

 * `$NAME` is a user-friendly name for the transition
 * `$BASE` is the path to a directory dedicated to the transition
 * `$CONFIG` is the path to a directory with configuration (e.g.
   a subdir of `examples`)

`$BASE` will be created if it does not exist, then populated. In the
following, everything will be done from there.

A convenient symbolic link `usr` to the parent directory of
`create-transition.sh` will be made. A rootfs tarball will be created
in `ben/rootfs.tar.zst`. The APT repository of the new universe will
be progressively built in `pool`. The rootfs is set up to use (and
prefer) it.

Source packages are taken from unstable by default. To override that,
put the source package in the `override` directory. Before a package
is built, a new changelog entry is added and a suffix is appended to
the version.

To initialize (or update) ben's working directory:

    make ben

To update the Release file of the new universe:

    make pool

To sign the Release file:

    make sign

Note that this is not necessary for the rootfs, it trusts the pool
unconditionally which is fine as packages are generated locally.

To update other files before exposing the repository to the world:

    make

To rebuild a specific package in the new universe:

    usr/bin/rebuild.sh $PACKAGE [sbuild options]

This will build the package in the rootfs, and put the result and log
in the `build` directory. Note that sbuild is always called with
`--source` and `--force-orig-source`; additional options (such as
`--no-arch-all`) can be given.

To move the result and log to `pool`:

    usr/bin/promote.sh $PACKAGE

To do a round of compilation of all possible packages:

    make rebuild

This may be very long! Additional options can be given to `ben
rebuild` via the `BEN_REBUILD_OPTS` variable. For example, `-j N`
allows parallelization with at most `N` simultaneous builds. You might
want to restrict parallelization of the builds themselves with
`OMP_THREAD_LIMIT=1` and/or `parallel=1` in `DEB_BUILD_OPTIONS`.

Additionally, if `BEN_REBUILD_TIMELIMIT` is set, its value will be
used to limit the time taken by a build (the `timelimit` package is
then needed).

For example:

    BEN_REBUILD_TIMELIMIT=600 BEN_REBUILD_OPTS="-j $(nproc)" DEB_BUILD_OPTIONS=parallel=1 make rebuild

Eventually, `missing.txt` will contain the list of failing packages and
their failing dependencies, in Makefile syntax.
