#!/bin/sh

set -e

PACKAGE=$1

if [ -d ../build ]; then
    cd ..
fi

has_debs () {
    [ -f "$1" ] && [ "$(dcmd --deb "$1" | wc -l)" -ge 1 ]
}

. ben/env.sh

cd build

CHANGES="$(find -name "$PACKAGE"'_*.changes')"
LOG_LINK="$(find -name "$PACKAGE"'_*'"$ARCH"'.build')"

if [ -L "$LOG_LINK" ]; then
    LOG="$(readlink "$LOG_LINK")"
    if [ -f "$LOG" ]; then
        mkdir -p ../pool/$PACKAGE
        mv "$LOG_LINK" "$LOG" ../pool/$PACKAGE
    fi
fi

if has_debs "$CHANGES"; then
    mkdir -p ../pool/$PACKAGE
    dcmd mv "$CHANGES" ../pool/$PACKAGE
fi
