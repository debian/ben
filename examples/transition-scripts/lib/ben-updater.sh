#!/bin/sh

set -e

if [ -d ../build ]; then
    cd ..
fi

if [ -n "$BEN_REBUILD_DONTUPDATE" ]; then
    exit 0
fi

touch pool/stamp && make -C pool -f ../usr/lib/Makefile.pool
