#!/bin/sh

set -e

BASE="$PWD"

. ben/env.sh

cat > ben/rebuilt.list <<EOF
deb [trusted=yes] file:///rebuilt ./
EOF

cat > ben/rebuilt.pref <<EOF
Package: *
Pin: release a=$SUITE
Pin-Priority: 1000
EOF

cat > ben/sbuild.conf <<EOF
\$chroot_mode = 'unshare';
\$chroot = '$BASE/ben/rootfs.tar.zst';
\$unshare_bind_mounts = [ { directory => "$BASE/pool", mountpoint => "/rebuilt" } ];
\$run_lintian = 0;
EOF

mmdebstrap --variant=buildd "$BASE_SUITE" \
  --components="main contrib non-free non-free-firmware" \
  --customize-hook='chroot "$1" mkdir /rebuilt' \
  --customize-hook="copy-in $BASE/ben/rebuilt.list /etc/apt/sources.list.d" \
  --customize-hook="copy-in $BASE/ben/rebuilt.pref /etc/apt/preferences.d" \
  "$BASE/ben/rootfs.tar.zst" "$MIRROR"
