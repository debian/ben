/*
  Copyright © 2009 Stéphane Glondu <steph@glondu.net>
  Copyright © 2009 Mehdi Dogguy <mehdi@dogguy.org>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Dependencies: jquery.
*/

var param_ids = ['#good', '#bad', '#partial', '#unknown', '#notintesting'];

function update_url () {
    var new_params = [];
    for (var i = 0; i < param_ids.length; i++) {
	var param = param_ids[i];
	new_params.push((($(param).is(":checked")) ? '' : '!') + param.slice(1));
    }
    url = '#' + new_params.join(',')
    window.location.assign(url);
}

function update () {
    $(".src").removeClass("mark");
    if ($("#good").is(":checked")) {
        $(".src").filter(".good").parent().show();
    } else {
        $(".src").filter(".good").addClass("mark").parent().hide();
    }
    if ($("#partial").is(":checked")) {
        $(".src").filter(".partial").parent().show();
    } else {
        $(".src").filter(".partial").addClass("mark").parent().hide();
    }
    if ($("#bad").is(":checked")) {
        $(".src").filter(".bad").parent().show();
    } else {
        $(".src").filter(".bad").addClass("mark").parent().hide();
    }
    if ($("#unknown").is(":checked")) {
        $(".src").filter(".unknown").parent().show();
    } else {
        $(".src").filter(".unknown").addClass("mark").parent().hide();
    }
    if ($("#notintesting").is(":checked")) {
        $(".src").filter(".notintesting").parent().hide();
    } else {
        $(".src").filter(".notintesting").each(function() {
            if(!$(this).hasClass("mark")) {
                $(this).parent().show();
            }
        });
    }
    for (var i = 0; i < nb_rounds; i++) {
        if ($(".round"+i).filter(":visible").length > 0) {
            $("#header"+i).show();
        } else {
            $("#header"+i).hide();
        }
    }
    $("#count").html(" ("+$(".srcname").filter(":visible").length+")");
    update_url();
};

function toggle() {
    var params = window.location.href.split('#')
    if (params.length > 1) {
	toggles = params[1].split(',');
	toggles.forEach(function (button) {
	    if (button == '' || button == '!') {
		return;
	    } else if (button.startsWith('!')) {
		b_id = '#' + button.slice(1);
		$(b_id).prop("checked", false);
	    } else {
		b_id = '#' + button;
		$(b_id).prop("checked", true);
	    }
	});
    }
}

function init () {
    $("#good").click(update);
    $("#partial").click(update);
    $("#bad").click(update);
    $("#unknown").click(update);
    $("#notintesting").click(update);
    toggle();
    update();
}

$(document).ready(init);
