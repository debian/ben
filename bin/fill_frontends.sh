#!/bin/bash

STATIC_FRONTENDS=""
for x in $(ls frontends/ben_*.ml); do
    x="${x#frontends/ben_}"
    x="${x%.ml}"
    x="Ben_frontends.Ben_$x.frontend"
    STATIC_FRONTENDS="$x; $STATIC_FRONTENDS"
done

exec sed -e "s/@STATIC_FRONTENDS@/$STATIC_FRONTENDS/g" "$@"
